import tensorflow as tf
import numpy as np
import scipy

def runModel(path):
    with tf.Session() as sess:
        saver = tf.train.import_meta_graph('./tmp/model.meta')
        saver.restore(sess, './tmp/model')

        y_ = tf.get_collection('y_')[0]
        x = tf.get_collection('x')[0]
        y = tf.get_collection('y')[0]

        data = np.vectorize(lambda x: x / 255.0)(np.ndarray.flatten(scipy.ndimage.imread(path, flatten=True)))
        mylist = list()
        mylist.append(data)
        d = np.array(mylist)

        batch_y = np.array([0., 0., 1., 0., 0., 0., 0., 0., 0., 0.])
        mylist1 = list()
        mylist1.append(batch_y)
        by = np.array(mylist1)

        result = sess.run(tf.argmax(y_,1), feed_dict={x: d, y: by})
        print("Result: ", result[0])
        return result[0]
