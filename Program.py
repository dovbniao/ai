import Train
import Classify
import os.path
from tkinter import *
from tkinter.filedialog import askopenfilename
from PIL import ImageTk, Image

#Main window

print("Welcome\n")
if os.path.exists('./tmp/model.meta'):
    trained = True
else:
    trained = False
prediction = False
filename = ""

class App:

    def __init__(self, mr):
        mr.title('Recognizing digits')

        frame = Frame(master=mr, width=600, height=500, bg='#E48BEE')
        frame.pack_propagate(0) #Don't allow the widgets inside to determine the frame's width / height
        frame.pack(fill=BOTH, expand=1) #Expand the frame to fill the root window

        buttonfr = Frame(master=frame, width=100, height=500, bg='#E48BEE')
        buttonfr.pack_propagate(0)
        buttonfr.pack(side=RIGHT)

        labelfr = Frame(master=frame, width=500, height=500, bg='#E48BEE')
        labelfr.pack_propagate(0)
        labelfr.pack(side=LEFT)

        self.w = Label(master=labelfr, width=500, height=500, bg='#E48BEE')
        self.w.pack(fill=BOTH)

        train = Button(master=buttonfr, text='Start Training', bg='#702CB5', fg='#FFFFFF', command=self.start_train)
        train.pack(fill=X)

        upload = Button(master=buttonfr, text='Upload Image', bg='#702CB5', fg='#FFFFFF', command=self.upload_image)
        upload.pack(fill=X)

        classify = Button(master=buttonfr, text='Classify Image', bg='#702CB5', fg='#FFFFFF', command=self.start_classification)
        classify.pack(fill=X)

        self.res = Label(master=buttonfr, text="Result!")
        self.res.pack(fill=X)



    def start_train(self):
        global trained
        global prediction
        if not prediction:
            Train.trainModel()
            trained = True
        else:
            print("Can't train after predicting started\n")

    def upload_image(self):
        global filename
        filename = askopenfilename(filetypes=(("Template files", "*.jpg")
                                                         , ("All files", "*.*")))
        self.image = ImageTk.PhotoImage(Image.open(filename).resize((300,300),Image.ANTIALIAS))
        self.w.config(image=self.image)


    def start_classification(self):
        global filename
        global prediction
        if trained:
            #addr = input("Please enter path to file: \n")
            result = Classify.runModel(filename)
            self.res.config(text=result)
            self.res.update()
            prediction = True
        else:
            print("Please perform training prior to classification\n")


root = Tk()

app = App(root)


#Tkinter event loop
root.mainloop()



# import Train
# import Classify
# import os.path
#
# print("Welcome\n")
# if os.path.exists('./tmp/model.meta'):
#     trained = True
# else:
#     trained = False
# prediction = False
#
# while 1:
#     print("Print 1 to start training\nPrint 2 to do classification\nPrint 0 to exit\n")
#     par1 = input()
#     if par1 == "1":
#         if not prediction:
#             Train.trainModel()
#             trained = True
#         else:
#             print("Can't train after predicting started\n")
#     elif par1 == "2":
#         if trained:
#             addr = input("Please enter path to file: \n")
#             Classify.runModel(addr)
#             prediction = True
#         else:
#             print("Please perform training prior to classification\n")
#     elif par1 == "0":
#         print("Thank you for using this program\n")
#         break
#     else:
#         continue