import cv2

def Preproc(data):
    gray = cv2.cvtColor(data, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(255 - gray, (28, 28))
    (thresh, gray) = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)